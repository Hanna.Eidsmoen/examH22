package INF102.examH22.solar;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * This class represents a list split in 2 parts, the low numbers and the high numbers.
 * When a new instance is created all numbers should be in the hi part.
 * The class implements the interface Iterator every time next() is called
 * one number will move from the high part to the low part.
 * The next number in the iterator defines the split point, 
 * i.e. all numbers in low are <= next() and all numbers in hi are >= next() 
 * 
 * @author Martin Vatshelle
 *
 */
public class SplitList implements Iterator<Integer> {

	private Collection<Integer> low; //the low numbers
	private Collection<Integer> hi; //the high numbers
	private Integer current;	
	
	/**
	 * Constructs a SplitList where all elements are in hi
	 * list can not be empty, at least 1 element needs to be provided
	 * @param list The list of numbers this class should keep track of
	 */
	public SplitList(List<Integer> list) { 
		low = new ArrayList<>();
		hi = new ArrayList<>(list);
		current = removeMin(hi);
	}

	//removes and returns the smallest element from hi
	private int removeMin(Collection<Integer> c) { 
		int min = Collections.min(c);
		c.remove(min);
		return min;
	}
	
	//returns the sum of all elements in low
	public int sumBelow() {
		return sum(low);
	}

	//returns the sum of all elements in hi
	public int sumAbove() {
		return sum(hi);
	}
	
	//returns the sum of all elements in the given collection
	private int sum(Collection<Integer> coll) {
		int sum = 0;
		for(int val : coll)
			sum += val;
		return sum;
	}
	
	//returns the number of elements in low
	public int numBelow() {
		return low.size();
	}

	//returns the number of elements in hi
	public int numAbove() { 
		return hi.size();
	}
	
	//returns the current element
	public int getCurrent() {
		return current;
	}

	@Override
	public boolean hasNext() { 
		return !hi.isEmpty();
	}

	//next() changes current to the smallest element in hi and returns the new current
	@Override
	public Integer next() {
		low.add(current);
		current = removeMin(hi);
		return current;
	}
	
	/**
	 * Adds a number to this SplitList. Current should not change so
	 * if num is smaller than current it needs to be added to low
	 * if num is >= current it will be added to hi.
	 * @param num the number to add
	 */
	public void add(int num) {
		if(num>=current)
			hi.add(num);
		else
			low.add(num);
	}

	/**
	 * This method removes the smallest element in this SplitList
	 * @return the element that was removed
	 */
	public int removeSmallest() {
		if(low.isEmpty()) {
			int smallest = current;
			current = removeMin(hi);
			return smallest;
		}
		return removeMin(low);
	}
}
